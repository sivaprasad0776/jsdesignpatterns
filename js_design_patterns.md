# JS Design Patterns
Design Patterns is a way to resolve common problems in the code development. It is also a solution to commonly occurring problems in software design. 

Design patterns can be split into three categories. 

* Creational - to create new things 
* Structural - to structure our code
* Behavioral - for behavior purposes 

## Creational patterns:
These are patterns that control the creation process of an object. These allow us to efficiently create new objects in JavaScript. 
Some of the creational design patterns are as follows:
* Constructor 
* Singleton
* Factory 
* Abstract Factory 

### constructor pattern : 
JavaScript supports special constructor functions that work with objects, byy  prefixing a call to a constructor function with the keyword new, we can tell JavaScript we would like the function to behave like a constructor and instantiate a new object with the members defined by that function.

Inside a constructor, the keyword this references the new object that’s being created. 

~~~Javascript
function Student( name, subject, marks ) {

    this.name = name;
    this.subject = subject;
    this.marks = marks;
  
    this.toString = function () {
      return ` Student by name ${this.name} has secured ${this.marks} marks in ${this.subject}` ;
    };
  }
  
  // Usage:
  
  // We can create new instances of the student
  var raman = new Student( "Raman", 'Physics', 95 );
  var lalitha = new Student( "Lalitha", "Maths", 94);

  console.log(raman.toString());
  console.log(lalitha.toString());
  ~~~



## Singleton Pattern:
Singleton Pattern prevents the from creating more than one instance of the function.  Singleton pattern can be implemented by creating an object with a method that creates a new instance of the function if one doesn’t exist. If an instance is already existing, it simply returns a reference to that object.

This patern is highly useful when we need a single instance like a connection object to the underlining databse. If we use this pattern in this kind of senarios it can enable us to use the resourses efficiently and effectively. 

Ex:

~~~Javascript
let dbConnection = function(){
    let obj = null;
    function init(db){
        if(obj == null){
            obj = `Connection object to ${db}`;
        }
        return obj;
    }
    return init;
}

let con = dbConnection();

let con1 = con('con-1');
console.log(con1);

// object is already initialised we will still get the reference to previous instance only.
let con2 = con('con-2');
console.log(con2);

~~~

## Factory Pattern: 
The factory pattern is used  when we want a mechanism that creates objects of its own.  This can be useful when you want a factory method to handle the creation of objects instead of using new method.  

it differs from the other patterns in the mechanism of creating the objects,   it doesn’t explicitly require us to use a constructor. Instead, this provide a generic interface for creating objects, where we can specify the type of factory object we wish to create.

~~~Javascript
let college = function(){
let Student = function( name, subject, marks ) {

    this.name = name;
    this.subject = subject;
    this.marks = marks;
  
    this.toString = function () {
      return ` Student by name ${this.name} has secured ${this.marks} marks in ${this.subject}` ;
    };
  }
  return{
    collegeStudent(name, subject, marks){
        let student = new Student(name, subject, marks);
        return student;

    }
  }
}

let mb = college();
let ram = mb.collegeStudent('Ram', 'Python', 98);
let shyam = mb.collegeStudent('Shyam', 'JS', 96);

let ramInfo = ram.toString();
let shyamInfo = shyam.toString();

console.log(`Student Ram information : ${ramInfo}`);
console.log(`Student Shyam information : ${shyamInfo}`);

~~~

## Structural Patterns:   
The structural pattern are focused on how to properly organize the structure of the larger application. 

Some of the structural patterns are as follows:
* Module
* Mixins
* Facade
* Flyweight
* Decorator
* MVC
* MVP
* MVVM

## Module Pattern:
The Module pattern was originally defined as a way to provide both private and public encapsulation for classes in conventional software engineering. 

It is just encapsulating a block of code into a singular function or a pure functio. This is to organize the code into pure functions. So if we need to debug the code, it will be much easier to find where the error is. 

This encapsulation result in shielding particular parts of the code from the global scope. This also resolve the variablee or function names conflicting with other functions defined inother parts of the code..

This mechanism is widely used in all of the modern javascript frameworks like React, Angular, Express.

In node the modularity is implemented im[plicitly by the node environment. We need to export and import the functions, variables in order to use them in other places ..

Ex: 
### Implementing the modularity in node environment:

In the following example, getDate(date) functionality is implemented in getdate.js which takes the valid date as a string and returns the date and it is exported to make it availbel in th eother modules. 

This functionality is imported into usedate.js file and the date is extracted by passing th edate as string to getdate function implemented in the other file getdate.js.

~~~Javascript
//getdate.js

const getDate = date => {
    if (date == null || date.length == 0 || typeof date !='string') {
        return 0;
    }
    else {
        // considering the date in dd/mm/yyyy format
        const re = /^(0?[1-9]|[12][0-9]|3[01])\/(0?[1-9]|1[12])\/(19|20)\d\d$/ ;
        if (!re.test(date)) {
            return 0;
        }
        else {
            const ids = date.split('/');
            
            return parseInt(ids[0]);
        }
    }

}

module.exports = getDate;


// usedate.js

let getDate = require('./getdate');

let date = getDate('12/12/2020');
console.log(date);
~~~



## Behavioral Patters:
These patterns are related to behaviors in our applications, they are focused on improving communications between objects in a system. It offers patterns that help standardize and implement communications in between pieces of code. 

Some of the Behavioral patterns include:
* Observe
* State
* Iterator
* Mediator
* command

### Iterator:

The iterator pattern provides a way to access the elements sequentially, of an aggregate object without exposing its underlying representation. This allow us to traverse through all the elements of a collection in an easy manner.

This mechanism is implemented in all the collection objects like arrays, sets, maps as well as in  objects to access the all key-value pairs without much hassle.

Eg:

~~~Javascript
let fruits = ['Mango', 'Banana', 'Pineapple', 'Apple', 'Grapes', 'Guava'];
let student = {
    name: 'Prasad',
    gender: 'Male',
    subject: 'Maths',
    marks: 90
};

// Iterating through array
fruits.forEach((fruit)=>{
    console.log(fruit);
});

// Iterating through object
for (const [key, value] of Object.entries(student)) {
    console.log(`${key} : ${value}`);

}

~~~

### References
* [Oreilly](https://www.oreilly.com/)
* [Medium](https://medium.com/front-end-weekly/javascript-design-patterns-ed9d4c144c81)
* [Linkedin Learning](https://www.linkedin.com/learning/javascript-patterns-2)